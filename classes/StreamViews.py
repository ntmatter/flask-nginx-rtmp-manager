from .shared import db

import datetime


class StreamViews(db.Model):
    __tablename__ = "stream_views"
    id = db.Column(db.Integer, primary_key=True)
    wpUser = db.Column(db.Integer)
    seconds = db.Column(db.Integer)
    subscription = db.Column(db.Integer)
    eventID = db.Column(db.Integer)
    channelLoc = db.Column(db.String(255), db.ForeignKey('Channel.channelLoc'))
    ip = db.Column(db.String(255))
    date = db.Column(db.DateTime)

    def __init__(self, wp_user, seconds, subscription, event_id, channel_loc, ip):
        self.wpUser = wp_user
        self.seconds = seconds
        self.subscription = subscription
        self.eventID = event_id
        self.channelLoc = channel_loc
        self.ip = ip
        self.date = datetime.datetime.now()

    def __repr__(self):
        return '<id %r>' % self.id

    def get_time(self):
        seconds = self.seconds % (24 * 3600)
        hour = seconds
        seconds %= 3600
        minutes = seconds
        seconds %= 60
        return "%d:%02d:%02d" % (hour, minutes, seconds)

    def get_minutes(self):
        return int(self.seconds / 60)

    def serialize(self):
        return {
            'id': self.id,
            'wpUser': self.wpUser,
            'seconds': self.seconds,
            'subscription': self.subscription,
            'minutes': int(self.seconds / 60),
            'eventID': self.eventID,
            'channelLoc': self.channelLoc,
            'ip': self.ip,
            'date': self.date.strftime("%d/%b/%Y %H:%M:%S.%f")
        }