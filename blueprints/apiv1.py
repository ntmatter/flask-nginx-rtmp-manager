import sys
from os import path, remove
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from flask import Blueprint, request, url_for
from flask_restplus import Api, Resource, reqparse

import shutil
import uuid

from classes import Sec
from classes import Channel
from classes import Stream
from classes import StreamViews
from classes import RecordedVideo
from classes import topics
from classes import upvotes
from classes import apikey
from classes import views
from classes import settings
from classes.shared import db

from globals import globalvars

class fixedAPI(Api):
    # Monkeyfixed API IAW https://github.com/noirbizarre/flask-restplus/issues/223
    @property
    def specs_url(self):
        '''
        The Swagger specifications absolute url (ie. `swagger.json`)

        :rtype: str
        '''
        return url_for(self.endpoint('specs'), _external=False)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api_v1 = Blueprint('api', __name__, url_prefix='/apiv1')
api = fixedAPI(api_v1, version='1.0', title='OSP API', description='OSP API for Users, Streamers, and Admins', default='Primary', default_label='OSP Primary Endpoints', authorizations=authorizations)

### Start API Functions ###

channelParserPut = reqparse.RequestParser()
channelParserPut.add_argument('channelName', type=str)
channelParserPut.add_argument('description', type=str)
channelParserPut.add_argument('topicID', type=int)

channelParserPost = reqparse.RequestParser()
channelParserPost.add_argument('channelName', type=str, required=True)
channelParserPost.add_argument('description', type=str, required=True)
channelParserPost.add_argument('topicID', type=int, required=True)
channelParserPost.add_argument('recordEnabled', type=bool, required=True)
channelParserPost.add_argument('chatEnabled', type=bool, required=True)
channelParserPost.add_argument('commentsEnabled', type=bool, required=True)

userViewsParserPut = reqparse.RequestParser()
userViewsParserPut.add_argument('channel', type=str)
userViewsParserPut.add_argument('eventID', type=int)
userViewsParserPut.add_argument('seconds', type=int)
userViewsParserPut.add_argument('subscription', type=int)

streamParserPut = reqparse.RequestParser()
streamParserPut.add_argument('streamName', type=str)
streamParserPut.add_argument('topicID', type=int)

videoParserPut = reqparse.RequestParser()
videoParserPut.add_argument('videoName', type=str)
videoParserPut.add_argument('description', type=str)
videoParserPut.add_argument('topicID', type=int)

clipParserPut = reqparse.RequestParser()
clipParserPut.add_argument('clipName', type=str)
clipParserPut.add_argument('description', type=str)

xmppAuthParserPost = reqparse.RequestParser()
xmppAuthParserPost.add_argument('jid', type=str)
xmppAuthParserPost.add_argument('token', type=str)

xmppIsUserParserPost = reqparse.RequestParser()
xmppIsUserParserPost.add_argument('jid', type=str)
# TODO Add Clip Post Arguments


@api.route('/server')
class api_1_Server(Resource):
    @staticmethod
    def get(self):
        """
            Displays a Listing of Server Settings
        """
        serverSettings = settings.settings.query.all()[0]
        db.session.commit()
        return {'results': serverSettings.serialize()}


@api.route('/channel/')
class api_1_ListChannels(Resource):
    @staticmethod
    def get():
        """
            Gets a List of all Public Channels
        """
        channelList = Channel.Channel.query.all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in channelList]}

    @staticmethod
    @api.expect(channelParserPost)
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def post():
        """
            Creates a New Channel
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    args = channelParserPost.parse_args()
                    newChannel = Channel.Channel(
                        int(requestAPIKey.userID),
                        str(uuid.uuid4()),
                        args['channelName'],
                        int(args['topicID']),
                        args['recordEnabled'],
                        args['chatEnabled'],
                        args['commentsEnabled'],
                        args['description']
                    )
                    db.session.add(newChannel)
                    db.session.commit()

                    return {'results': {'message': 'Channel Created', 'apiKey': newChannel.streamKey}}, 200
        return {'results': {'message': 'Request Error'}}, 400


@api.route('/channel/<string:channelEndpointID>')
@api.doc(params={
    'channelEndpointID': 'Channel Endpoint Descriptor, Expressed in a UUID Value(ex:db0fe456-7823-40e2-b40e-31147882138e)'
})
class api_1_ListChannel(Resource):
    def get(self, channelEndpointID):
        """
            Get Info for One Channel
        """
        channelList = Channel.Channel.query.filter_by(channelLoc=channelEndpointID).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in channelList]}
    # Channel - Change Channel Name or Topic ID
    @api.expect(channelParserPut)
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def put(self, channelEndpointID):
        """
            Change a Channel's Name or Topic
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    channelQuery = Channel.Channel.query.filter_by(
                        channelLoc=channelEndpointID,
                        owningUser=requestAPIKey.userID
                    ).first()
                    if channelQuery is not None:
                        args = channelParserPut.parse_args()
                        if 'channelName' in args:
                            if args['channelName'] is not None:
                                channelQuery.channelName = args['channelName']
                        if 'description' in args:
                            if args['description'] is not None:
                                channelQuery.description = args['channelName']
                        if 'topicID' in args:
                            if args['topicID'] is not None:
                                possibleTopics = topics.topics.query.filter_by(id=int(args['topicID'])).first()
                                if possibleTopics is not None:
                                    channelQuery.topic = int(args['topicID'])
                        db.session.commit()
                        return {'results': {'message':'Channel Updated'}}, 200
        return {'results': {'message':'Request Error'}},400

    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def delete(self, channelEndpointID):
        """
            Deletes a Channel
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    channelQuery = Channel.Channel.query.filter_by(
                        channelLoc=channelEndpointID,
                        owningUser=requestAPIKey.userID
                    ).first()
                    if channelQuery is not None:
                        videos_root = globalvars.videoRoot + 'videos/'
                        filePath = videos_root + channelQuery.channelLoc
                        if filePath != videos_root:
                            shutil.rmtree(filePath, ignore_errors=True)

                        channelVid = channelQuery.recordedVideo
                        channelUpvotes = channelQuery.upvotes
                        channelStreams = channelQuery.stream

                        for entry in channelVid:
                            for upvote in entry.upvotes:
                                db.session.delete(upvote)
                            vidComments = entry.comments
                            for comment in vidComments:
                                db.session.delete(comment)
                            vidViews = views.views.query.filter_by(viewType=1, itemID=entry.id)
                            for view in vidViews:
                                db.session.delete(view)
                            db.session.delete(entry)
                        for entry in channelUpvotes:
                            db.session.delete(entry)
                        for entry in channelStreams:
                            db.session.delete(entry)
                        db.session.delete(channelQuery)
                        db.session.commit()
                        return {'results': {'message': 'Channel Deleted'}}, 200
        return {'results': {'message': 'Request Error'}}, 400


@api.route('/stream/')
class api_1_ListStreams(Resource):
    @staticmethod
    def get():
        """
             Returns a List of All Active Streams
        """
        streamList = Stream.Stream.query.all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in streamList]}


@api.route('/stream_views/<int:eventID>')
@api.doc(params={'eventID': 'ID Number for the Event'})
class api_1_ListStreamViews(Resource):
    @staticmethod
    def get(eventID):
        """
             Returns View Info on a Single Event
        """
        streamViewsList = StreamViews.StreamViews.query.filter_by(eventID=eventID).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in streamViewsList]}


@api.route('/user_views/<int:wpUser>')
@api.doc(params={'wpUser': 'ID Number for the User'})
class api_1_ListStreamViews(Resource):
    @staticmethod
    def get(wpUser):
        """
             Returns View Info on a Single Event
        """
        streamViewsList = StreamViews.StreamViews.query.filter_by(wpUser=wpUser).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in streamViewsList]}

    @staticmethod
    @api.expect(userViewsParserPut)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def put(wpUser):
        args = userViewsParserPut.parse_args()
        channelLoc = str(args['channel'])
        eventID = abs(int(args['eventID']))
        seconds = abs(int(args['seconds']))
        subscription = abs(int(args['subscription']))
        user_exists = False
        try:
            ip = request.environ["HTTP_X_FORWARDED_FOR"].split(",")[0].strip()
        except (KeyError, IndexError):
            ip = request.remote_addr

        requestedChannel = Channel.Channel.query.filter_by(channelLoc=channelLoc).first()
        requestedChannel.secondsViewed = requestedChannel.secondsViewed + seconds
        requestedStream = StreamViews.StreamViews.query.filter_by(eventID=eventID, wpUser=wpUser).all()

        if requestedStream is []:
            new_stream = StreamViews.StreamViews(
                wp_user=wpUser,
                seconds=seconds,
                subscription=subscription,
                event_id=eventID,
                channel_loc=channelLoc,
                ip=ip
            )
            db.session.add(new_stream)
        elif wpUser is 0:
            ip_exists = False
            for user_ip in requestedStream:
                if user_ip.ip == ip:
                    ip_exists = True
                    user_ip.seconds += seconds
                    break
            if ip_exists is False:
                new_stream = StreamViews.StreamViews(
                    wp_user=wpUser,
                    seconds=seconds,
                    subscription=subscription,
                    event_id=eventID,
                    channel_loc=channelLoc,
                    ip=ip
                )
                db.session.add(new_stream)
        else:
            for user_ip in requestedStream:
                if user_ip.wpUser is wpUser:
                    user_ip.seconds = user_ip.seconds + seconds
                    user_exists = True
                    break
            if user_exists is False:
                new_stream = StreamViews.StreamViews(
                    wp_user=wpUser,
                    seconds=seconds,
                    subscription=subscription,
                    event_id=eventID,
                    channel_loc=channelLoc,
                    ip=ip
                )
                db.session.add(new_stream)
        db.session.commit()
        db.session.close()
        return {'results': {'IP': ip, 'WPUser': wpUser, 'seconds': seconds, 'subscription': subscription}}


@api.route('/stream/<int:streamID>')
@api.doc(params={'streamID': 'ID Number for the Stream'})
class api_1_ListStream(Resource):
    @staticmethod
    def get(streamID):
        """
             Returns Info on a Single Active Streams
        """
        streamList = Stream.Stream.query.filter_by(id=streamID).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in streamList]}
        # Channel - Change Channel Name or Topic ID

    @staticmethod
    @api.expect(channelParserPut)
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def put(streamID):
        """
            Change a Streams's Name or Topic
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    streamQuery = Stream.Stream.query.filter_by(id=int(streamID)).first()
                    if streamQuery is not None:
                        if streamQuery.channel.owningUser == requestAPIKey.userID:
                            args = streamParserPut.parse_args()
                            if 'streamName' in args:
                                if args['streamName'] is not None:
                                    streamQuery.streamName = args['streamName']
                            if 'topicID' in args:
                                if args['topicID'] is not None:
                                    possibleTopics = topics.topics.query.filter_by(id=int(args['topicID'])).first()
                                    if possibleTopics is not None:
                                        streamQuery.topic = int(args['topicID'])
                            db.session.commit()
                            return {'results': {'message': 'Stream Updated'}}, 200
        return {'results': {'message': 'Request Error'}}, 400


@api.route('/video/')
class api_1_ListVideos(Resource):
    @staticmethod
    def get():
        """
             Returns a List of All Recorded Videos
        """
        videoList = RecordedVideo.RecordedVideo.query.filter_by(pending=False, published=True).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in videoList]}


@api.route('/video/<int:videoID>')
@api.doc(params={'videoID': 'ID Number for the Video'})
class api_1_ListVideo(Resource):
    @staticmethod
    def get(videoID):
        """
             Returns Info on a Single Recorded Video
        """
        videoList = RecordedVideo.RecordedVideo.query.filter_by(id=videoID, published=True).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in videoList]}

    @api.expect(videoParserPut)
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def put(self, videoID):
        """
            Change a Video's Name, Description, or Topic
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    videoQuery = RecordedVideo.RecordedVideo.query.filter_by(id=int(videoID)).first()
                    if videoQuery is not None:
                        if videoQuery.owningUser == requestAPIKey.userID:
                            args = videoParserPut.parse_args()
                            if 'videoName' in args:
                                if args['videoName'] is not None:
                                    videoQuery.channelName = args['videoName']
                            if 'topicID' in args:
                                if args['topicID'] is not None:
                                    possibleTopics = topics.topics.query.filter_by(id=int(args['topicID'])).first()
                                    if possibleTopics is not None:
                                        videoQuery.topic = int(args['topicID'])
                            if 'description' in args:
                                if args['description'] is not None:
                                    videoQuery.description = args['description']
                            db.session.commit()
                            return {'results': {'message': 'Video Updated'}}, 200
        return {'results': {'message': 'Request Error'}}, 400

    @staticmethod
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def delete(videoID):
        """
            Deletes a Video
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    videoQuery = RecordedVideo.RecordedVideo.query.filter_by(id=videoID).first()
                    if videoQuery is not None:
                        if videoQuery.owningUser == requestAPIKey.userID:
                            videos_root = globalvars.videoRoot + 'videos/'

                            filePath = videos_root + videoQuery.videoLocation
                            thumbnailPath = videos_root + videoQuery.videoLocation[:-4] + ".png"

                            if filePath != videos_root:
                                if path.exists(filePath) and (
                                        videoQuery.videoLocation is not None or videoQuery.videoLocation != ""):
                                    remove(filePath)
                                    if path.exists(thumbnailPath):
                                        remove(thumbnailPath)
                            upvoteQuery = upvotes.videoUpvotes.query.filter_by(videoID=videoQuery.id).all()
                            for vote in upvoteQuery:
                                db.session.delete(vote)
                            vidComments = videoQuery.comments
                            for comment in vidComments:
                                db.session.delete(comment)
                            vidViews = views.views.query.filter_by(viewType=1, itemID=videoQuery.id)
                            for view in vidViews:
                                db.session.delete(view)
                            db.session.delete(videoQuery)
                            db.session.commit()
                            return {'results': {'message': 'Video Deleted'}}, 200
        return {'results': {'message': 'Request Error'}}, 400


@api.route('/clip/')
class api_1_ListClips(Resource):
    @staticmethod
    def get():
        """
             Returns a List of All Saved Clips
        """
        clipsList = RecordedVideo.Clips.query.filter_by(published=True).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in clipsList]}


@api.route('/clip/<int:clipID>')
@api.doc(params={'clipID': 'ID Number for the Clip'})
class api_1_ListClip(Resource):
    @staticmethod
    def get(clipID):
        """
             Returns Info on a Single Saved Clip
        """
        clipList = RecordedVideo.Clips.query.filter_by(id=clipID, published=True).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in clipList]}

    @staticmethod
    @api.expect(clipParserPut)
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def put(clipID):
        """
            Change a Clip's Name or Description
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    clipQuery = RecordedVideo.Clips.query.filter_by(id=int(clipID)).first()
                    if clipQuery is not None:
                        if clipQuery.recordedVideo.owningUser == requestAPIKey.userID:
                            args = clipParserPut.parse_args()
                            if 'clipName' in args:
                                if args['clipName'] is not None:
                                    clipQuery.clipName = args['clipName']
                            if 'description' in args:
                                if args['description'] is not None:
                                    clipQuery.description = args['description']
                            db.session.commit()
                            return {'results': {'message': 'Clip Updated'}}, 200
        return {'results': {'message': 'Request Error'}}, 400

    @staticmethod
    @api.doc(security='apikey')
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def delete(clipID):
        """
            Deletes a Clip
        """
        if 'X-API-KEY' in request.headers:
            requestAPIKey = apikey.apikey.query.filter_by(key=request.headers['X-API-KEY']).first()
            if requestAPIKey is not None:
                if requestAPIKey.isValid():
                    clipQuery = RecordedVideo.Clips.query.filter_by(id=clipID).first()
                    if clipQuery is not None:
                        if clipQuery.owningUser == requestAPIKey.userID:
                            videos_root = globalvars.videoRoot + 'videos/'
                            thumbnailPath = videos_root + clipQuery.thumbnailLocation

                            if thumbnailPath != videos_root:
                                if path.exists(thumbnailPath) and clipQuery.thumbnailLocation is not None and clipQuery.thumbnailLocation != "":
                                    remove(thumbnailPath)
                            upvoteQuery = upvotes.clipUpvotes.query.filter_by(clipID=clipQuery.id).all()
                            for vote in upvoteQuery:
                                db.session.delete(vote)

                            db.session.delete(clipQuery)
                            db.session.commit()
                            return {'results': {'message': 'Clip Deleted'}}, 200
        return {'results': {'message': 'Request Error'}}, 400


@api.route('/topic/')
class api_1_ListTopics(Resource):
    @staticmethod
    def get():
        """
             Returns a List of All Topics
        """
        topicList = topics.topics.query.all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in topicList]}


@api.route('/topic/<int:topicID>')
@api.doc(params={'topicID': 'ID Number for Topic'})
class api_1_ListTopic(Resource):
    @staticmethod
    def get(topicID):
        """
             Returns Info on a Single Topic
        """
        topicList = topics.topics.query.filter_by(id=topicID).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in topicList]}


@api.route('/user/<string:username>')
@api.doc(params={'username': 'Username of OSP User'})
class api_1_ListUser(Resource):
    @staticmethod
    def get(username):
        """
            Get Public Info for One User
        """
        userQuery = Sec.User.query.filter_by(username=username).all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in userQuery]}


@api.route('/xmpp/auth')
@api.doc(params={'jid': 'JID of user', 'token': 'Jabber Token'})
class api_1_xmppAuth(Resource):
    @staticmethod
    @api.expect(xmppAuthParserPost)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def post():
        """
        Verify Chat Authentication
        """
        args = xmppAuthParserPost.parse_args()
        if 'jid' in args:
            jid = args['jid']
            if 'token' in args:
                token = args['token']
                sysSettings = settings.settings.query.first()
                if sysSettings is not None:
                    username = jid.replace("@" + sysSettings.siteAddress,"")
                    userQuery = Sec.User.query.filter_by(username=username, active=True).first()
                    if userQuery != None:
                        if userQuery.xmppToken == token:
                            return {'results': {'message': 'Successful Authentication', 'code': 200}}, 200
        return {'results': {'message': 'Request Error', 'code': 400}}, 400


@api.route('/xmpp/isuser')
@api.doc(params={'jid': 'JID of user'})
class api_1_xmppisuser(Resource):
    @staticmethod
    @api.expect(xmppIsUserParserPost)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def post():
        """
        Verify if User
        """
        args = xmppIsUserParserPost.parse_args()
        if 'jid' in args:
            jid = args['jid']
            sysSettings = settings.settings.query.first()
            if sysSettings is not None:
                username = jid.replace("@" + sysSettings.siteAddress, "")
                userQuery = Sec.User.query.filter_by(username=username).first()
                if userQuery != None:
                    return {'results': {'message': 'Successful Authentication', 'code': 200}}, 200
        return {'results': {'message': 'Request Error', 'code': 400}}, 400
